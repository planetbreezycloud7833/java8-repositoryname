package com.modernjava.funcprograming;


import java.util.Optional;
import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.stream.IntStream;

public class NumericStreamAggregateExample {
    public static void main(String[] args) {


        int sum = IntStream.rangeClosed(0, 1000).sum();
        System.out.println(sum);

        OptionalInt min=IntStream.rangeClosed(0,1000).min();
        if(min.isPresent())
            System.out.println(min.getAsInt());

        OptionalInt max=IntStream.rangeClosed(0,1000).max();
        if(max.isPresent())
            System.out.println("Max of 1000 numbers is :"+ max.getAsInt());

        //avg
        OptionalDouble average=IntStream.rangeClosed(0,1000).average();
        System.out.println("average is " + (average.isPresent()?average.getAsDouble():0.0));

    }
}