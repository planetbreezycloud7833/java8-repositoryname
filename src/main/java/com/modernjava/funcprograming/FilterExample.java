package com.modernjava.funcprograming;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class FilterExample {
    public static void main(String[] args) {
        //Returning instructors sorted by their name and have more than 10 years of experience
        List<Instructor> list=Instructors.getAll()
                .stream()
                .filter(i -> i.getYearsOfExperience()>10)
                .sorted(Comparator.comparing(Instructor::getName))
                .collect(Collectors.toList());
        System.out.println(list);
  //      list.forEach(System.out::println);
    }
}
