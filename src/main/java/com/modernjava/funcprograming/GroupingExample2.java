package com.modernjava.funcprograming;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GroupingExample2 {
    public static void main(String[] args) {

        List<String> name= List.of("Sid","Mike","Jenny","Gene","Rajeev");
        Map<Integer,List<String>> result=name.stream().collect(Collectors.groupingBy(String::length,Collectors
                .filtering(s ->s.contains("e"),Collectors.toList())));
        System.out.println("result = " + result);

        //Instructor grouping them by Senior(>10) and Junior(<10) and filter them by online courses

        Map<String,List<Instructor>> instructorByExpAndOnline =Instructors.getAll()
                .stream().collect(Collectors.groupingBy(instructor ->
                        instructor.getYearsOfExperience()>10 ? "SENIOR" :"JUNIOR",
                        Collectors.filtering(s -> s.isOnlineCourses(),Collectors.toList())));

        instructorByExpAndOnline.forEach((key,value ) -> {
            System.out.println("key  = " + key +" value= " + value);
        });

    }
}
