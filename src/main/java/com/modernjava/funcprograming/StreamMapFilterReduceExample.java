package com.modernjava.funcprograming;

public class StreamMapFilterReduceExample {
    public static void main(String[] args) {
        //Total years of experience b/w instructors
        int result=Instructors.getAll().stream()
                .filter(Instructor::isOnlineCourses)
                .map(Instructor::getYearsOfExperience)
//                .reduce(0,(a,b) -> a+b);
                .reduce(0,Integer::sum);
        System.out.println(result);
    }
}
