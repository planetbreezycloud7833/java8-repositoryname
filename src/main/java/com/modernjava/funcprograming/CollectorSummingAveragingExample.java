package com.modernjava.funcprograming;

import java.util.stream.Collectors;

public class CollectorSummingAveragingExample {
    public static void main(String[] args) {
        //Sum of years of experience of all instructor
        int sum = Instructors.getAll().stream().collect(Collectors.summingInt(Instructor::getYearsOfExperience));
        System.out.println("sum = " + sum);

        //Calculate average of years of experience of all the instructors

        double average=Instructors.getAll().stream().collect(Collectors.averagingInt(Instructor::getYearsOfExperience));
        System.out.println("Average = "+average);
    }
}
