package com.modernjava.funcprograming;

import java.util.List;
import java.util.function.Consumer;

public class ConsumerExample2 {
    public static void main(String[] args) {
        List<Instructor> instructors = Instructors.getAll();
        Consumer<Instructor> c1= (s1) -> System.out.println(s1);
        instructors.forEach(c1);


        //Loop through all the instructor and only print out their names
       Consumer<Instructor> c2=(s1) -> System.out.println(s1.getName());
       instructors.forEach(c2);
       System.out.println("-----------");

       //Loop through all the Instructors and print out their names and their courses
       Consumer<Instructor> c3=(s1) ->System.out.println(s1.getCourses());
       instructors.forEach(c2.andThen(c3));
       System.out.println("-----------");

        //Loop through all the Instructors  and print out their names if the years of experience is >10
        instructors.forEach(s1 ->{
            if(s1.yearsOfExperience>10){
                c1.accept(s1);
            }
        });
        System.out.println("--------");

        //Loop through all the instructors and print out their names and years of experience if users of experience is >5 and teaches course online
        instructors.forEach(s1 ->{
            if(s1.yearsOfExperience>5 && !s1.onlineCourses==true){
                c1.andThen(c2).accept(s1);
            }
        });
        System.out.println("--------");
    }
}
