package com.modernjava.funcprograming;

import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Collectors;

public class CollectorsMinMaxExample {
    public static void main(String[] args) {
        //Instructor with minimum years of experience
        Optional<Instructor> instructor=Instructors.getAll().stream()
                .collect(Collectors.minBy(Comparator.comparing(Instructor::getYearsOfExperience)));
        System.out.println("Instructor = "+instructor);

        System.out.println("--------------------------------");
        
        instructor=Instructors.getAll().stream().min(Comparator.comparing(Instructor::getYearsOfExperience));
        System.out.println("instructor = " + instructor);

        
    }
}
