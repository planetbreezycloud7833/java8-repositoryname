package com.modernjava.funcprograming;



import java.util.function.Function;

public class FunctionExample {
    public static void main(String[] args) {
        Function<String,String>  lowercaseFunction = (s) -> s.toLowerCase();
        System.out.println(lowercaseFunction.apply("PROGRAMING"));

        Function<String,String> concatFunction=(s) -> s.concat(" In Java");
        System.out.println(lowercaseFunction.andThen(concatFunction).apply("PROGRAMING"));

        System.out.println(lowercaseFunction.compose(concatFunction).apply("PROGRAMING"));


    }


}
