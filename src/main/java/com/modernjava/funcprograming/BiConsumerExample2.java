package com.modernjava.funcprograming;

import java.util.List;
import java.util.function.BiConsumer;

public class BiConsumerExample2 {
    public static void main(String[] args) {
        List<Instructor> instructors=Instructors.getAll();
        System.out.println("--------");
        //Print out the names and gender of instructors
        BiConsumer<String,String> biConsumer=(name,gender) -> System.out.println("name is :"+ name +" and gender is: "+gender);
        instructors.forEach(instructor -> {
            biConsumer.accept(instructor.getName(),instructor.getGender());
        });
        System.out.println("--------");

        //Print out the names and list of all the courses
        BiConsumer<String,List<String>> biConsumer1= (name,courses) -> System.out.println("name is :"+ name + "courses: "+ courses);
        instructors.forEach(instructor ->{
            biConsumer1.accept(instructor.getName(),instructor.getCourses());
        });
        System.out.println("--------");

        //Print out the name and gender of all the instructors who teaches online
        instructors.forEach(instructor ->{
            if(instructor.isOnlineCourses())
            biConsumer.accept(instructor.getName(),instructor.getGender());
        });

        //Print out the name and gender and their courses of all the instructors who teaches online
    }
}
